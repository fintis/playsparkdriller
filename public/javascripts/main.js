/**
 * Created by osas on 11/9/14.
 */

$(document).ready(function () {

    var options = {
         series: {
         shadowSize: 0	// Drawing is faster without shadows
         },
        lines: {
            show: true
        },
        points: {
            show: true
        },
        xaxis: {
            tickDecimals: 0,
            tickSize: 1,
            position: "top"
        },
        yaxis: {
            transform: function (v) {
                return -v;
            },
            inverseTransform: function (v) {
                return -v;
            }
        }
    };

var updateInterval = 1000;
    var dataUrl;
    var data = [] ;


    var button = $("#driller");

    button.on("click", function () {
         dataUrl = button.attr("path");
        fetchData();

    });


    $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");

    function fetchData() {
        $.ajaxSetup({ cache: false });

        $.ajax({
            url: dataUrl,
            type: "GET",
            dataType: "json",
            success:update,
            error: function () {
                setTimeout(fetchData, updateInterval);
            }

        });

    }

    function update(_data) {
        data.shift();

        for (var i = 0; i < _data.length; i++) {
            var obj = _data[i];

            data.push([obj.rop, obj.depth]);
        }

        console.log(data);
        var plotData = [
            { label: "ROP", data: data, color: "#000" }
        ];
        $.plot("#placeholder", plotData , options);
        setTimeout(fetchData, updateInterval);
    }


});


