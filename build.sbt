name := "Spark Driller"

version := "1.0-SNAPSHOT"

val sparkVersion = "1.1.0"

libraryDependencies ++= Seq(
  //Akka
  "com.typesafe.akka" %% "akka-actor" % "2.2.3",
  "com.typesafe.akka" %% "akka-slf4j" % "2.2.3",
  // Spark and Spark Streaming
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-streaming-kafka" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  // Kafka
  "org.apache.kafka" %% "kafka" % "0.8.1.1"
    exclude("javax.jms", "jms")
    exclude("com.sun.jdmk", "jmxtools")
    exclude("com.sun.jmx", "jmxri"),
  //"com.101tec" % "zkclient" % "0.4",
  // for serialization of case class
  "com.novus" %% "salat" % "1.9.8",
  // Joda dates for Scala
 // "com.github.nscala-time" %% "nscala-time" % "1.2.0",
  //Cassandra
  "com.datastax.spark" %% "spark-cassandra-connector" % "1.1.0-beta1" withSources() withJavadoc(),
  "org.apache.cassandra" % "cassandra-thrift" % "2.1.0",
  "com.datastax.spark"  %% "spark-cassandra-connector-embedded" % sparkVersion  excludeAll(ExclusionRule("org.slf4j")) excludeAll(ExclusionRule("org.apache.spark")) excludeAll(ExclusionRule("com.typesafe")) excludeAll(ExclusionRule("org.apache.cassandra")) excludeAll(ExclusionRule("com.datastax.cassandra"))// ApacheV2
)

play.Project.playScalaSettings

offline := true
