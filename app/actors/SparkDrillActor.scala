/*
 *  Copyright (C) Algomacro Solutions Nig, Ltd - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Osabuohien Osahon <jeff.osahon@algomacro.com>, 2015.
 *
 */

package actors

import java.util.Properties

import akka.actor.Actor.Receive
import akka.actor.{Props, Actor, ActorLogging}
import kafka.javaapi.producer.Producer
import kafka.producer.ProducerConfig
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import play.api.libs.concurrent.Akka
import utils.{DrillOptimizerUtil, Constants}
import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Play.current

/**
 * Created by osahon on 1/5/2015.
 */

case class StreamInitialized()
case class CassandraInitialize()

class SparkDrillActor extends Actor with ActorLogging {

  /**Size of output batches in seconds**/
  val outputBatchInterval = sys.env.get("OUTPUT_BATCH_INTERVAL").map(_.toInt).getOrElse(1)

  /**
   * Configure Spark
   */
  val conf = new SparkConf(true).
    setMaster("local[4]").
    setAppName("Spark Drill OptimizerII").
    setSparkHome("SPARK_HOME").
    set("spark.executor.memory", "512m").
    set("spark.cleaner.ttl", (outputBatchInterval * 43200).toString).
    set("spark.cleaner.delay", (outputBatchInterval * 43200).toString).
    set("spark.cassandra.connection.host", "127.0.0.1")

  /** Get Spark context **/
  val sc = getSparkContext
  /** Get streaming context **/
  val ssc = new StreamingContext(sc, Seconds(outputBatchInterval))

  //val kafka = new EmbeddedKafka
  /**
   * Configure Kafka parameters
   */
  val kafkaParams = Map(
    "zookeeper.connect" -> "localhost:2181",
    "zookeeper.connection.timeout.ms" -> "10000",
    "group.id" -> "sparkDriller"
  )

  /** Get kafka topic **/
  val topics = Map(
    Constants.KafkaTopic -> 1
  )
  //kafka.createTopic(Constants.KafkaTopic)
  /**
   * Configure Kafka serializer
   */
  val props = new Properties()
  props ++= Map(
    "serializer.class" -> "kafka.serializer.DefaultEncoder",
    "key.serializer.class" -> "kafka.serializer.StringEncoder",
    "metadata.broker.list" -> "127.0.0.1:9092"
  )
  /** Instantiate Kafka producer **/
  val config = new ProducerConfig(props)
  val producer = new Producer[String, Array[Byte]](config)
  //val producer = new KafkaProducer[String, String](kafka.kafkaConfig)

  /** instantiate Actor to respond to asynchronous messages for drill simulation **/
  val drillStreamActor = Akka.system.actorOf(Props(new DrillStreamingActor(ssc, producer, kafkaParams,  Map(Constants.KafkaTopic -> 1), self)), "Drill-Stream")

  lazy val drillSimActor = Akka.system.actorOf(Props(new DrillSimulatorActor(ssc, producer,  Map(Constants.KafkaTopic -> 1))), "Drill-Simulate")
  Akka.system.scheduler.schedule(0 seconds, 1 seconds, drillSimActor, DrillStart())


  def getSparkContext = new SparkContext(conf)



  override def receive: Receive = {
    case e: StreamInitialized =>
      log.info("Stream started")
      ssc.start()
    case e: CassandraInitialize =>
      log.info(s"Creating Cassandra tables...")
    DrillOptimizerUtil.createCassandraTables(ssc)
  }
}
