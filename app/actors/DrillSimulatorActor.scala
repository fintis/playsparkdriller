package actors

import akka.actor.{ActorLogging, Actor, Cancellable}
import com.datastax.driver.core.utils.UUIDs
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.embedded.{EmbeddedKafka, KafkaProducer}
import com.datastax.spark.connector.streaming._
import kafka.javaapi.producer.Producer
import kafka.producer.{KeyedMessage}
import kafka.serializer.StringDecoder
import kafka.utils.Json
import models.{DrillLog, DrillLogOptimized}
import org.apache.log4j.Logger
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka.KafkaUtils
import utils._
import utils.Constants._

case class DrillStart()

/*case class DrillStop(ref: Cancellable)

case class DrillStream()

case class Kill()

case class Initialize()

case class GetOptimizedData()

case class Result(data: Array[DrillLogOptimized])*/

class DrillSimulatorActor(ssc: StreamingContext, producer: Producer[String, Array[Byte]],topic: Map[String, Int]) extends Actor with ActorLogging{


  var counter = 0
  var depth = Depth

  // infinite loop
  private def drill() = {
    /**initialize a unique timestamp for Cassandra storage**/
    val timestamp = UUIDs.timeBased().toString

    /**get the lines read from the file loaded via SparkContext and count variable pointing to current row**/
    val row = getLinesAndCount._1.collect().apply(counter)

    /**create DrillLog object with each column of the row/line read**/
    DrillLog(timestamp, row(0), row(1), row(3), row(4), row(5), row(6), row(7), row(8))
  }




  /**
   * overriden recieve method of an actor: Here, messages are handled in a non-blocking fashion based on Scala's case class type test
   * similar to Java's switch statements
   *
   */
  override def receive: Receive = {
    case msg: DrillStart =>
      val drillLog = drill()

      /** Serialize to Json **/
     val serialized = DrillOptimizerUtil.serialize(drillLog)
      log.info(serialized.toString)
      //val producer = new KafkaProducer[String, String](kafka.kafkaConfig)
      producer.send(new KeyedMessage[String, Array[Byte]](Constants.KafkaTopic, serialized))
      //producer.send(Constants.KafkaTopic, "rop-topic-group", jsonString)
      counter = counter + 1
      if (counter == 29) counter = 0


  }



  /**
   * get optimized data from cassandra
   * @return an Array of Drill data
   */
  /*private def readOptimizedDrillData() = {

    val cRDD = ssc.sparkContext.cassandraTable[DrillLogOptimized]("spark_drill_opt", "drill_log_optimized")

    cRDD.collect() // collect in local array may not be the most efficient on a larger scale but this is okay for now
  }

  private def gracefulShutDown() = {
    //kafka.shutdown()
    ssc.stop(stopSparkContext = true, stopGracefully = true)
  }*/

  private def getLinesAndCount = {
    //read text file for use in creating a pseudo drill stream simulation
    val sc = ssc.sparkContext
    val lines = sc.textFile("public/data/sample-drill-data.txt").map(_.split(" ").map(_.toDouble))
    val count = lines.count().toInt
    (lines, count)
  }
}

