/*
 *  Copyright (C) Algomacro Solutions Nig, Ltd - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Osabuohien Osahon <jeff.osahon@algomacro.com>, 2015.
 *
 */

package actors

import akka.actor.{ActorRef, ActorLogging, Actor}
import akka.serialization.SerializationExtension
import com.novus.salat
import com.novus.salat.Context
import com.novus.salat.global._
import kafka.javaapi.producer.Producer
import kafka.serializer.{DefaultDecoder, StringDecoder}
import models.{DrillLogOptimized, DrillLog}
import org.apache.commons.io.Charsets
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.{GeneralizedLinearModel, LabeledPoint}
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{StreamingContext, Seconds}
import org.apache.spark.streaming.kafka.KafkaUtils
import play.api.libs.concurrent.Akka
import play.api.libs.json.Json
import utils.{DrillOptimizerUtil, StreamingRidgeRegressionWithLBFGS}
import com.datastax.spark.connector.streaming._
import play.api.Play.current

import scala.math._

/**
 * Created by osahon on 1/5/2015.
 */
class DrillStreamingActor(
                           ssc: StreamingContext,
                           producer: Producer[String, Array[Byte]],
                           kafkaParams: Map[String, String],
                          // utils: DrillOptimizerUtil,
                           topics: Map[String, Int],
                           listener: ActorRef) extends Actor with ActorLogging {

  /** read file from local directory **/
  val file = ssc.sparkContext.textFile("public/data/sample-drill-data.txt")
  /** and create batch training set **/
  val batchTrainingSet = DrillOptimizerUtil.initBatchData(file)
  /** train model from batch training set using LBFS algorithm. This will serve to get initial weights for use in the streaming computation **/
  val (startingWeightAndIntercept, loss) = DrillOptimizerUtil.trainWithLBFGS(batchTrainingSet, Vectors.dense(new Array[Double](batchTrainingSet.first()._2.size)))
  log.info(s"Starting batch weight and intercept :$startingWeightAndIntercept")

  /**
   * Begin streaming computation reading data streams from Kafka producers. Streams are converted into labels and features for real-time
   * regression training using initial weights and intercept from batch file. Results are also classified in real-time using SVM offline model
   * eventually written to Cassandra in real-time after optimized WOB and RPM are computed
   */

  /** stream of (topic, DrillLog) **/
  log.info("Initializing stream...")

  val messages = KafkaUtils.createStream[String, Array[Byte], StringDecoder, DefaultDecoder](ssc, kafkaParams, topics, StorageLevel.MEMORY_ONLY).cache()


  val streamTrainingSet = DrillOptimizerUtil.createFromStream(messages)// transform stream into dstream B&Y transformation
  val labelAndFeatures = streamTrainingSet.map(dStream => LabeledPoint(dStream.head, MLUtils.appendBias(Vectors.dense(dStream.tail))))
  StreamingRidgeRegressionWithLBFGS.setInitialWeights(startingWeightAndIntercept)
  StreamingRidgeRegressionWithLBFGS.trainOn(labelAndFeatures)


  ctx.registerClassLoader(current.classloader)

  messages.map { rawData =>
    salat.grater[DrillLog].fromJSON(new String(rawData._2, Charsets.UTF_8))
  }.saveToCassandra("spark_drill_opt", "drill_log")//store incoming data to cassandra
  messages.window(Seconds(5), Seconds(1)).map {//process last 5 seconds of data every second
     drillData =>

       val fromJson = salat.grater[DrillLog].fromJSON(new String(drillData._2, Charsets.UTF_8))

      val drillLog: Map[String, Double] = Map(
         "depth" -> fromJson.depth,
         "wob" -> fromJson.wob,
         "rpm" -> fromJson.rpm,
         "toothWear" -> fromJson.toothWear,
         "reyNum" -> fromJson.reyNum,
         "ecd" -> fromJson.ecd,
         "ppg" -> fromJson.ppg)

       val rop = DrillOptimizerUtil.calculateROP(StreamingRidgeRegressionWithLBFGS.latestModel, drillLog)
       val wob = DrillOptimizerUtil.getOptimizedWOB(StreamingRidgeRegressionWithLBFGS.latestModel)
       val rpm = DrillOptimizerUtil.getOptimizedRPM(wob)

       DrillLogOptimized(fromJson.timeStamp, rop, wob, rpm, fromJson.depth, 0.0)

   }.saveToCassandra("spark_drill_opt", "drill_log_optimized") //store optimized data

  listener ! StreamInitialized()


  def receive: Actor.Receive = {
    case e => //
  }
}
