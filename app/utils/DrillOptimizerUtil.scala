package utils


import com.novus.salat
import com.novus.salat.Context
import com.novus.salat.global._
import org.apache.commons.io.Charsets
import play.api.libs.json.Json

import scala.math._

import akka.serialization.SerializationExtension
import com.datastax.spark.connector.cql.CassandraConnector
import models.DrillLog
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.optimization.{LBFGS, LeastSquaresGradient, SquaredL2Updater}
import org.apache.spark.mllib.regression.GeneralizedLinearModel
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import play.api.Play.current
import play.api.libs.concurrent.Akka

object DrillOptimizerUtil extends Serializable {


  private def BourgoyneYoungModel(params: Map[String, Double]): Array[Double] = {
    val y = log(params("rop"))
    val x2 = 10000 - params("depth")
    val x3 = pow(params("depth"), 0.69) * (params("ppg") - 9)
    val x4 = params("depth") * (params("ppg") - params("ecd"))
    val x5 = log((params("wob") - 0.02) / (4 - 0.02))
    val x6 = log(params("rpm") / 100)
    val x7 = params("toothWear") * (-1)
    val x8 = params("reyNum")
    Array(y, x2, x3, x4, x5, x6, x7, x8)

  }

  def calculateROP(model: GeneralizedLinearModel, row: Map[String, Double]) = {
    //println(model.weights.toVector)
    //drilling constants from multiple regression after Bourgoyne and Young 1974
    val constants = model.weights.toArray // transform weight vector to array
    val a1 = model.intercept
    val a2 = constants(0)
    val a3 = constants(1)
    val a4 = constants(2)
    val a5 = constants(3)
    val a6 = constants(4)
    val a7 = constants(5)
    val a8 = constants(6)

    //val wob = getOptimizedWOB(a5, a6)
    //val rpm = getOptimizedRPM(wob)

    val f1 = exp(a1)
    val f2 = exp(a2 * (10000 - row("depth")))
    val f3 = exp(a3 * pow(row("depth"), 0.69) * (row("ppg") - 9))
    val f4 = exp(a4 * (row("depth") * (row("ppg") - row("ecd"))))
    val f5 = exp(a5 * log((row("wob") - 0.02) / (4 - 0.02)))
    //val f5 = exp(a5 * log((wob - 0.02) / (4 - 0.02)))
    val f6 = exp(a6 * log(row("rpm") / 100))
    //val f6 = exp(a6 * log(rpm / 100))
    val f7 = exp(a7 * (row("toothWear") * (-1)))
    val f8 = exp(a8 * row("reyNum"))

    f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8
  }

  def initBatchData(file: RDD[String]) = {
    val wellData = file //sc.textFile(file) //read in supplied file
    //wellData.foreach(x =>println(x))
    val data = wellData.map(_.split(" ").map(_.toDouble))
    //read one line at a time tokenizing on spaces
    data.map {
      row => //read one array at a time
        val batch: Map[String, Double] = Map("rop" -> row(0),
          "depth" -> row(1),
          "wob" -> row(3),
          "rpm" -> row(4),
          "toothWear" -> row(5),
          "reyNum" -> row(6),
          "ecd" -> row(7),
          "ppg" -> row(8))
        val transformedData = BourgoyneYoungModel(batch) //transform features according to Bourgoyne and Young 1974 drilling model

        (transformedData.head, MLUtils.appendBias(Vectors.dense(transformedData.tail)))
    }.cache()
  }

  /**
   * register the gratar serializer classloader for play
   */
  ctx.registerClassLoader(current.classloader)
  /**
   * Serialize Drillog object to JSON
   * param drillLog DrillLog case class object
   * @return JSON string representation
   */
  val serialize = (decoded: DrillLog) => {

    salat.grater[DrillLog].toCompactJSON(decoded).getBytes(Charsets.UTF_8)
  }

  /**
   * Deserialize Json string to DrillLog object
   * param jsonString JSON string sent in
   * @return DrillLog object
   */
  val deserialize = (encoded: Array[Byte]) => {
    salat.grater[DrillLog].fromJSON(new String(encoded, Charsets.UTF_8))
  }

  def createFromStream(msg: DStream[(String, Array[Byte])]) = {
    msg.map {
      log =>
        /** deserialize JSON to DrillLog object **/
        val drillLog = deserialize(log._2)

        val stream: Map[String, Double] = Map("rop" -> drillLog.rop,
          "depth" -> drillLog.depth,
          "wob" -> drillLog.wob,
          "rpm" -> drillLog.rpm,
          "toothWear" -> drillLog.toothWear,
          "reyNum" -> drillLog.reyNum,
          "ecd" -> drillLog.ecd,
          "ppg" -> drillLog.ppg)
        val byModel = BourgoyneYoungModel(stream)
        // val scalar = new StandardScaler(withMean = true, withStd = true).fit(toRDDVector(byModel))
        byModel //.map(values => (values.head, Vectors.dense(values.tail)))

    }
  }

  val numCorrections = 10
  val convergenceTol = 1e-4
  val maxNumIterations = 50
  val regParam = 0.1

  def trainWithLBFGS(trainData: RDD[(Double, Vector)], initWeightAndIntercept: Vector) = {
    //val numFeatures = trainData.first()._2.size
    val initialWeightsWithIntercept = initWeightAndIntercept
    //println(numFeatures)
    LBFGS.runLBFGS(
      trainData,
      new LeastSquaresGradient(),
      new SquaredL2Updater(),
      numCorrections,
      convergenceTol,
      maxNumIterations,
      regParam,
      initialWeightsWithIntercept)
  }

  val WOB_max = 8.0
  val H1 = 1.84
  val H3 = 0.8

  def getOptimizedWOB (model: GeneralizedLinearModel) = {
    val constants = model.weights.toArray
    val a5 = constants(3)
    val a6 = constants(4)
    val WOB_t = 0.5
    (a5 * H1 * WOB_max + a6 * WOB_t) / a5 * H1 + a6


  }

  def getOptimizedRPM (WOB_opt: Double) = {
    val tH = 15.7
    val tb = 16.1
    pow(tH / tb * (WOB_max - (WOB_opt / 1000)) / H3 * (WOB_max - 4), 1 / H1) * 100

  }
  def createCassandraTables(ssc: StreamingContext): Unit = {

    CassandraConnector(ssc.sparkContext.getConf).withSessionDo { session =>
      session.execute(s"DROP KEYSPACE IF EXISTS spark_drill_opt")
      session.execute(s"CREATE KEYSPACE IF NOT EXISTS spark_drill_opt WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1 }")
      session.execute(s"CREATE TABLE IF NOT EXISTS spark_drill_opt.drill_log (time_stamp varchar PRIMARY KEY, rop double, depth double, wob double, rpm double, tooth_wear double, rey_num double, ecd double, ppg double)")
      session.execute(s"CREATE TABLE IF NOT EXISTS spark_drill_opt.drill_log_optimized (time_stamp varchar PRIMARY KEY, rop double, wob double, rpm double, depth double, score double)")
      session.execute(s"TRUNCATE spark_drill_opt.drill_log")
      session.execute(s"TRUNCATE spark_drill_opt.drill_log_optimized")
    }
  }

}
