package utils

object Constants {

 //drilling simulation
  val MaxRop = 38.6
  val MinRop = 10
  val MaxWob = 3.76
  val MinWob = 0.81
  val MinRpm = 60
  val MaxRpm = 129
  val MinToothWear = 0.08
  val MaxToothWear = 0.77
  val MinReyNum = 0.201
  val MaxReyNum = 0.964
  val MinEcd = 9
  val MaxEcd = 14.1
  val MinPpg = 9
  val MaxPpg = 16
  val Depth = 8000.0
  //kafka topic
  val KafkaTopic = "rop-topic"
  val numPartitions = 8
  val replicationFactor = 1
  //zkclient
  val sessionTimeoutMs = 10000
  val connectionTimeoutMs = 10000


}
