package utils

import org.apache.spark.Logging
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression._
import org.apache.spark.streaming.dstream.DStream

object StreamingRidgeRegressionWithLBFGS extends Serializable with Logging {

  /**
   * Construct a StreamingLinearRegression object with default parameters:
   * {stepSize: 0.1, numIterations: 50, miniBatchFraction: 1.0}.
   * Initial weights must be set before using trainOn or predictOn
   * (see `StreamingLinearAlgorithm`)
   */
  //def this() = this(8, 10, 1e-4, 50, 0.1, Vectors.zeros(8))
  private var numFeatures: Int = 8
  private var numCorrections: Int = 10
  private var convergenceTol: Double = 1e-4
  private var maxNumIterations: Int = 50
  private var regParam: Double = 0.1
  private var initialWeightsWithIntercept: Vector = Vectors.zeros(8)

  var model = new RidgeRegressionModel(Vectors.dense(initialWeightsWithIntercept.toArray.slice(0, initialWeightsWithIntercept.size - 1)),
    initialWeightsWithIntercept.toArray(initialWeightsWithIntercept.size - 1))

  /**
   * Update the model by training on batches of data from a DStream.
   * This operation registers a DStream for training the model,
   * and updates the model based on every subsequent
   * batch of data from the stream.
   *
   * @param trainingSet DStream containing labeled data
   */
  def trainOn(trainingSet: DStream[LabeledPoint]) {
    if (Option(model.weights) == None) {
      logError("Initial weights must be set before starting training")
      throw new IllegalArgumentException
    }
    trainingSet.map(ds => (ds.label, ds.features)).foreachRDD {
      (rdd, time) =>
        val (weightsWithIntercept, loss) = DrillOptimizerUtil.trainWithLBFGS(rdd, initialWeightsWithIntercept)
        model = new RidgeRegressionModel(Vectors.dense(weightsWithIntercept.toArray.slice(0, weightsWithIntercept.size - 1)),
          weightsWithIntercept.toArray(weightsWithIntercept.size - 1))
        logInfo("Model updated at time %s".format(time.toString()))
        val display = model.weights.size match {
          case x if x > 100 => model.weights.toArray.take(100).mkString("[", ",", "...")
          case _ => model.weights.toArray.mkString("[", ",", "]")
        }
        logInfo("Current model: weights, %s".format(display))
    }


  }


  /** Set the step size for gradient descent. Default: 0.1. */
  def setStepSize(stepSize: Double): this.type = {
    this.convergenceTol = stepSize
    this
  }

  /** Set the number of iterations of gradient descent to run per update. Default: 50. */
  def setMaxNumIterations(numIterations: Int): this.type = {
    this.maxNumIterations = numIterations
    this
  }

  /** Set the initial weights. Default: [0.0, 0.0]. */
  def setInitialWeights(initialWeightsWithIntercept: Vector): this.type = {
    this.initialWeightsWithIntercept = initialWeightsWithIntercept
    this
  }

  /** @return model. */
  def latestModel: RidgeRegressionModel = model

}
