import actors.{CassandraInitialize, SparkDrillActor}
import akka.actor.Props
import play.api._
import play.api.libs.concurrent.Akka
import play.api.Play.current

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    Logger.info("Application has started")

    /** initialize System **/
    //lazy val manager =
      Akka.system.actorOf(Props(classOf[SparkDrillActor]))
   // manager ! CassandraInitialize()

  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")

  }

}
