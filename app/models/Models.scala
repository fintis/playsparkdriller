package models

import play.api.libs.json.Json

case class DrillLog(timeStamp: String, rop: Double, depth: Double, wob: Double, rpm: Double, toothWear: Double, reyNum: Double, ecd: Double, ppg: Double) extends Serializable

case class DrillLogOptimized(timeStamp: String, rop: Double, wob: Double, rpm: Double, depth: Double, score: Double) extends Serializable

object DrillLog {
  implicit val drillLogFormat = Json.format[DrillLog]
}

object DrillLogOptimized {
  implicit val drillLogOptimizedFormat = Json.format[DrillLogOptimized]
}