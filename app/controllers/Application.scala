package controllers

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.Cancellable
import play.api.mvc._

object Application extends Controller {

  var simulator: Cancellable = null

  def index = Action { implicit request =>

    Ok(views.html.index(""))
  }

  def start = Action { implicit request =>

    /** schedule drill simulation every 1 second using Akka scheduler **/
    //simulator = Akka.system.scheduler.schedule(0 seconds, 100 milliseconds, DrillOptimizer.drillSimActor, DrillStart())
    Status(200)
  }

  def fetchData = Action { implicit request =>
    //implicit val timeout = Timeout(5 seconds)
    //val optimizedData = DrillOptimizer.drillSimActor ? GetOptimizedData
    //val result = Await.result(optimizedData, timeout.duration).asInstanceOf[Array[DrillLogOptimized]]
    //Future.successful(Ok(Json.toJson(result)))
    Ok("")
  }

  def stop = Action { implicit request =>

    /** Stop the simulation sending a reference to the Scheduler **/
    //DrillOptimizer.drillSimActor ! DrillStop(simulator)
    Status(200)
  }

  def initStreaming = Action{ implicit request =>
    //DrillOptimizer.drillSimActor ! DrillStream()
    Future{

    }
    Status(200)
  }

  def exit = Action { implicit request =>
    System.exit(0)
    Status(503)
  }
}
